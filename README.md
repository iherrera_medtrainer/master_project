#Master example project Symfony 4.2

![](https://img.shields.io/badge/Symfony-4.2-blue.svg)
![PHP from Packagist](https://img.shields.io/packagist/php-v/symfony/symfony.svg)

This is a master project to use as template to develop new compatible projects with the new architecture scheme.
It includes:

- Base config for session handler (Redis* or PDOSessionHandler)
- Pre-loaded medcore bundle. (MedTrainer core & connector)
- Pre-loaded user provider to fetch the session and share it between SF 4.x projects.
- Basic login implementation using GUARD.
 
** To install Redis on Ubuntu/Debian you should open a terminal an execute: 

`sudo apt-get install redis-server` 

** You can also download Redis engine for Windows 10 [here](https://github.com/rgl/redis/downloads)

Also you need to download and install PHP Redis extension:

- If you are using Mac/Linux, you just need to run the following command: 
`sudo apt-get install php7.1-redis`. _Change 7.1 instead your PHP version_
- If you are using Windows, follow these steps:
    - Download **_php_redis.dll_** file from [PECL libs](https://pecl.php.net/package/redis/2.2.7/windows). Download extension as per your current PHP version.
    - Copy the **_php_redis.dll_** and paste to following folder in XAMPP Server extension directory (**C:\XAMPP\php\ext**).
    - Open the **_php.ini_** file and add _**extension=php_redis.dll**_ line into this file.
    - Restart XAMPP server
    - Open phpinfo() of XAMPP server and search Redis, if found that means you successfully integrated Redis with PHP.

## Installation

In order to use this project you need to follow these steps:

1. Rename the file _auth.json.dist_ to _auth.json_.
2. Open _auth.json_ and change the dummy params, using your **Bitbucket** credentials.
3. Open CMD/Terminal and execute `composer install`
4. Configure your database params in the _.env_ file

To launch embed server you just need to run `php bin/console server:run`

If you won't be able to use/configure Redis, you can still use _PDOSessionHandler_. 

To enable this feature you need to locate **_framework.yml_** file inside of **_config/packages_** folder
and uncomment _PDOSessionHandler_ line below handler_id and comment the _RedisSessionHandler_ one.

Locate **_services.yml_** under config folder and set the right params for PDO.