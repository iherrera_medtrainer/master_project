#Apache 2.4 configuration

In order to make lms works along with SF 4.x versions, you need to add several
configurations in your httpd.conf (apache2.conf on unix).

## MacOS - Linux

Assuming you have installed apache2 (not xampp, mampp), you need to install php 5.6 and 7.x. Just run:

```
sudo su
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install libapache2-mod-fastcgi php5.6-fpm php5.6 php5.6-dev php5.6-mcrypt php5.6-mbstring php5.6-mysql php5.6-zip php5.6-gd php5.6-xml 
php7.1-fpm libapache2-mod-fastcgi php7.1-fpm php7.1 php7.1-dev php7.1-mbstring php7.1-mysql php7.1-zip php7.1-gd php7.1-xml php7.1-curl php7.1-intl php7.1-json php7.1-mcrypt
```
** You can install another php7 version just replacing _1_ to x version you want to install, i.e. _php7.2-mbstring_

Once ready, just enable actions and fastcgi, running these commands.

```
a2enmod actions 
a2enmod fastcgi
```

Create a new file under _/etc/apache2/conf-available/_ named **_default_vhosts.conf_** and modify its content like this way:

```
<IfModule mod_fastcgi.c>
 AddHandler php56-fcgi-www .php
 Action php56-fcgi-www /php56-fcgi-www
 Alias /php56-fcgi-www /usr/lib/cgi-bin/php56-fcgi-www
 FastCgiExternalServer /usr/lib/cgi-bin/php56-fcgi-www -socket /run/php/php5.6-fpm.sock -pass-header Authorization
 <Directory "/usr/lib/cgi-bin">
  Require all granted
 </Directory>
</IfModule>
<IfModule mod_fastcgi.c>
 AddHandler php71-fcgi-www .php
 Action php71-fcgi-www /php71-fcgi-www
 Alias /php71-fcgi-www /usr/lib/cgi-bin/php71-fcgi-www
 FastCgiExternalServer /usr/lib/cgi-bin/php71-fcgi-www -socket /run/php/php7.1-fpm.sock -idle-timeout 1800 -pass-header Authorization
 <Directory "/usr/lib/cgi-bin">
  Require all granted
 </Directory>
</IfModule>
<IfModule mod_fastcgi.c>
 <FilesMatch ".+\.ph(p[345]?|t|tml)$">
  SetHandler php56-fcgi-www
 </FilesMatch>
</IfModule>
```

Enable this configuration, running `a2enconf default_vhosts.conf` (considering you still have superuser session enabled)

Restart apache `service apache2 restart`

Your environment is almost ready to run projects under _/var/www/html/_. By default, **every project will run using php 5.6**

You can add new php7 sites modifying _000-default.conf_ under _/etc/apache2/sites-enabled/_ and adding these lines:

```
<Directory /var/www/html/project_name>
	<FilesMatch ".+\.ph(p[345]?|t|tml)$">
		SetHandler php71-fcgi-www
	</FilesMatch>
</Directory>
```

** You need to restart apache every time you edit a configuration file, in order to force apache to show new changes.

## Windows 10

Assuming you have xampp installed (bundle php5.6) you need to download your preferred version of php7 [here](https://windows.php.net/download/).
**Download the non-thread safe package**

1. Extract the zip under your xampp installation (C:\xampp\php71)
2. Open the _php.ini_ which is located inside of _php71_ folder and uncomment the following line:
`extension_dir = "ext"`
3. Open xampp control panel, then press config button and select _http-xampp.conf_. A text file will open up. Just put the following
settings at the bottom of the file: 

```
ScriptAlias /php71 "C:/xampp/php71"
Action application/x-httpd-php71-cgi /php71/php-cgi.exe
<Directory "C:/xampp/php71">
    AllowOverride None
    Options None
    Require all denied
    <Files "php-cgi.exe">
        Require all granted
    </Files>
</Directory>
```

Save the file and restart apache.

** If you are facing _malformed header_ issue, open the file again and comment this line: `SetEnv PHPRC "\\path\\to\\xampp\\php"`.
 
In order to run any php7 application, open the file _httpd.conf_ (same way as _http-xampp.conf_) and put these lines at the end of the file:

```
<Directory "C:\xampp\htdocs\my_project_folder">
    <FilesMatch "\.php$">
        SetHandler application/x-httpd-php71-cgi
    </FilesMatch>
</Directory>
```

**Keep in mind you need to restart apache after any change.**